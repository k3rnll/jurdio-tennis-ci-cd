FROM maven:3.9-eclipse-temurin-17 as build
COPY repo/ /app/
WORKDIR /app
RUN mvn package

FROM tomcat:jre17
COPY --from=build /app/target/tenis-score-board-1.0-SNAPSHOT.war $CATALINA_HOME/webapps/
CMD ["catalina.sh", "run"]
